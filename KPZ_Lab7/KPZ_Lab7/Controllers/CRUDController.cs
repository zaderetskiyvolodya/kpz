﻿using KPZ_Lab7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace KPZ_Lab7.Controllers
{
    public class CRUDController : Controller
    {
        // GET: CRUD
        public ActionResult Index()
        {
            IEnumerable<Customers> cusObject = null;
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");

            var consumeApi = hc.GetAsync("Customer");
            consumeApi.Wait();

            var readData = consumeApi.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<IList<Customers>>();
                displayData.Wait();

                cusObject = displayData.Result;
            }
            return View(cusObject);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Customers cus)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");

            var insertRecord = hc.PostAsJsonAsync<Customers>("Customer", cus);
            insertRecord.Wait();

            var saveData = insertRecord.Result;
            if (saveData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        public ActionResult Details(int id)
        {
            Customer cus = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");

            var consumeApi = hc.GetAsync("Customer?id=" + id.ToString());
            consumeApi.Wait();

            var readData = consumeApi.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<Customer>();
                displayData.Wait();
                cus = displayData.Result;
            }
            return View(cus);
        }

        public ActionResult Edit(int id)
        {
            Customer cus = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");

            var consumeApi = hc.GetAsync("Customer?id=" + id.ToString());
            consumeApi.Wait();

            var readData = consumeApi.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<Customer>();
                displayData.Wait();
                cus = displayData.Result;
            }
            return View(cus);
        }

        [HttpPost]
        public ActionResult Edit(Customer cus)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");
            var insertRecord = hc.PutAsJsonAsync<Customer>("Customer", cus);
            insertRecord.Wait();

            var saveData = insertRecord.Result;
            if (saveData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.message = "Customer Data Not Updated";
            }
            return View(cus);
        }

        public ActionResult Delete(int id)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44329/api/Customer");

            var delRecord = hc.DeleteAsync("Customer/" + id.ToString());
            delRecord.Wait();

            var displayData = delRecord.Result;
            if (displayData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Index");
        }
    }
}