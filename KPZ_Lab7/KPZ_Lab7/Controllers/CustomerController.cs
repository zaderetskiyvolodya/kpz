﻿using KPZ_Lab7.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KPZ_Lab7.Controllers
{
    public class CustomerController : ApiController
    {
        ExursionAgencyEntities agencyEntities = new ExursionAgencyEntities();


        public IHttpActionResult GetCustomers()
        {
            var customers = agencyEntities.Customers.ToList();
            return Ok(customers);
        }

        [HttpPost]
        public IHttpActionResult InsertCustomer(Customers cus)
        {
            agencyEntities.Customers.Add(cus);
            agencyEntities.SaveChanges();
            return Ok();
        }

        public IHttpActionResult GetCustomer(int id)
        {
            Customer cusDetails = null;
            cusDetails = agencyEntities.Customers.Where(x => x.Id == id).Select(x => new Customer()
            {
                id = x.Id,
                name = x.name,
                surname = x.surname,
                age = x.age,
                exursions_ordered = x.exursions_ordered
            }).FirstOrDefault<Customer>();
            if (cusDetails == null)
            {
                return NotFound();
            }
            return Ok(cusDetails);
        }

        public IHttpActionResult PutCustomer(Customer cus)
        {
            var updateCus = agencyEntities.Customers.Where(x => x.Id == cus.id).FirstOrDefault<Customers>();
            if(updateCus != null)
            {
                updateCus.name = cus.name;
                updateCus.surname = cus.surname;
                updateCus.age = cus.age;
                updateCus.exursions_ordered = cus.exursions_ordered;
                agencyEntities.SaveChanges();
            }
            else
            {
                return NotFound();
            }
            return Ok();
        }

        public IHttpActionResult DeleteCustomer(int id)
        {
            var delCus = agencyEntities.Customers.Where(x => x.Id == id).FirstOrDefault<Customers>();
            agencyEntities.Entry(delCus).State = System.Data.Entity.EntityState.Deleted;
            agencyEntities.SaveChanges();
            return Ok();
        }

    }
}
