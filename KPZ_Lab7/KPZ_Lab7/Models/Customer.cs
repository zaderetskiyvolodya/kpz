﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPZ_Lab7.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int? age { get; set; }
        public int? exursions_ordered { get; set; }
    }
}