﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SampleMVVM.Models;
using SampleMVVM.Commands;
using System.Windows.Input;
using System.Media;
using SampleMVVM.Views;

namespace SampleMVVM.ViewModels
{
    public class ExursionTicketViewModel : ViewModelBase
    {
        public ExursionTicket TicketBooking;

        public ExursionTicketViewModel(ExursionTicket NewTicketBook)
        {
            this.TicketBooking = NewTicketBook;
        }

        public string Cashier
        {
            get { return TicketBooking.Cashier; }
            set
            {
                TicketBooking.Cashier = value;
                OnPropertyChanged("Cashier");
            }
        }

        public string Customer
        {
            get { return TicketBooking.Customer; }
            set
            {
                TicketBooking.Customer = value;
                OnPropertyChanged("Customer");
            }
        }

        public int TicketID
        {
            get { return TicketBooking.TicketID; }
            set
            {
                TicketBooking.TicketID = value;
                OnPropertyChanged("TicketID");
            }
        }

        public int Price
        {
            get { return TicketBooking.Price; }
            set
            {
                TicketBooking.Price = value;
                OnPropertyChanged("Price");
            }
        }

        #region Commands

        #region 
        private DelegateCommand clearCommand;

        public ICommand ClearCommand
        {
            get
            {
                if (clearCommand == null)
                {
                    clearCommand = new DelegateCommand(Clear, CanClear);
                }
                return clearCommand;
            }
        }

        private void Clear()
        {
            /*  Cashier = "";
              Customer = "";
              TicketID = 0;
              Price = 0;*/
            MainViewModel.Clear(TicketID);

        }

        private bool CanClear()
        {
            return TicketID > 0 || Price > 0 || Cashier != "" || Customer != "";
        }

        #endregion

        #region 
        private DelegateCommand PlayCommand;

        public ICommand playCommand
        {
            get
            {
                if (PlayCommand == null)
                {
                    PlayCommand = new DelegateCommand(Play);
                }
                return PlayCommand;
            }
        }

        private void Play()
        {
            System.Reflection.Assembly assembly =
                System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream resourceStream =
                assembly.GetManifestResourceStream(@"SampleMVVM.Astronomia.wav");
            SoundPlayer player = new SoundPlayer(resourceStream);
            player.Play();

        }

        #endregion

        #endregion
    }
}
