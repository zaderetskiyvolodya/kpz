﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

using SampleMVVM.Commands;
using System.Collections.ObjectModel;
using SampleMVVM.Models;

namespace SampleMVVM.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
         public static ObservableCollection<ExursionTicketViewModel> TicketBookingList { get; set; } 

        #region Constructor

        public MainViewModel(List<ExursionTicket> books)
        {
            TicketBookingList = new ObservableCollection<ExursionTicketViewModel>(books.Select(b => new ExursionTicketViewModel(b)));
        }

        #endregion

        public static void Clear(int _id)
        {
            foreach (var item in TicketBookingList.ToList<ExursionTicketViewModel>())
            {
                if(item.TicketID == _id)
                {
                    TicketBookingList.Remove(item);
                }
            } 
        }
    }
}
