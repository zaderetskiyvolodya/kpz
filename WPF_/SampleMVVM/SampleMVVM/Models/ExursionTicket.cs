﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Models
{
    public class ExursionTicket
    {
        public string Cashier { get; set; }
        public string Customer { get; set; }
        public int TicketID { get; set; }
        public int Price { get; set; }

        public ExursionTicket(string employeeName, string clientName, int ticket, int price)
        {
            this.Cashier = employeeName;
            this.Customer = clientName;
            this.TicketID = ticket;
            this.Price = price;
        }
    }
}
