﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using SampleMVVM.Models;
using SampleMVVM.ViewModels;
using SampleMVVM.Views;

namespace SampleMVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public List<ExursionTicket> ticketBookings;
        private void OnStartup(object sender, StartupEventArgs e)
        {
              ticketBookings = new List<ExursionTicket>()
            {
                new ExursionTicket("Petrovich", "Ivanovich", 5550, 250),
                new ExursionTicket("Pablo Escobar", "Misha Jackson", 5551, 120),
                new ExursionTicket("Handanovich", "Mario Getze", 5552, 340),
                new ExursionTicket("Vladislavovich", "Marco Polo", 5553, 340)
            };

            MainView view = new MainView();
            MainViewModel viewModel = new ViewModels.MainViewModel(ticketBookings); 
            view.DataContext = viewModel;
            view.Show();
        }


    }
}
