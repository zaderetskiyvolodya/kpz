﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVVMTest.Model
{
    public class Personage
    {
        public Personage(){
            Name = "Tanya";
            Bonus = 1000;
        }

        public string Name {get;set;}
        public int Bonus { get; set; }

        public delegate void CollectEventHandler(object source, MapEventArgs args);

        public class MapEventArgs : EventArgs
        {
            public int CardID { get; set; }
        }
    
    }
}
