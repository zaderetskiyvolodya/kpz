﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVVMTest.Model;
using System.Windows.Input;
using MVVMTest.ViewModel;
using SampleMVVM.Commands;

namespace MVVMTest.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            Person = new Personage();

            //StartAnimation = new DelegateCommand(arg => ClickMethod());


        }
        public Personage Person { get; set; }


        public ICommand StartAnimation { get; set; }
    }
}
