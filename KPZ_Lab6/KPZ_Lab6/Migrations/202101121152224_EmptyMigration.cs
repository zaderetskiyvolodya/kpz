﻿namespace KPZ_Lab6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmptyMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        surname = c.String(),
                        age = c.Int(),
                        exursions_ordered = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Exursions",
                c => new
                    {
                        name = c.String(nullable: false, maxLength: 128),
                        location_id = c.Int(nullable: false),
                        price = c.Single(nullable: false),
                        duration = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.name);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        city = c.String(),
                        street = c.String(),
                        house_number = c.String(),
                        longtitude = c.Single(),
                        latitude = c.Single(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Locations");
            DropTable("dbo.Exursions");
            DropTable("dbo.Customers");
        }
    }
}
