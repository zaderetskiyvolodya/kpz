﻿using KPZ_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KPZ_Lab6
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static public List<Customer> list;
        public MainWindow view;
        public App()
        {
            using (var context = new MyDBContext())
            {
                var query = from c in context.Customers select c;
                list = query.ToList<Customer>();
            
                

                view = new MainWindow();
                MainWindowModel viewModel = new ViewModels.MainWindowModel(list);
                view.DataContext = viewModel;
                view.Show();
            }
        }

        
    }
}
