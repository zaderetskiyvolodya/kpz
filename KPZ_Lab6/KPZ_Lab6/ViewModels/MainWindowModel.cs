﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

using KPZ_Lab6.Commands;
using System.Collections.ObjectModel;


namespace KPZ_Lab6.ViewModels
{
    public class MainWindowModel : ViewModelBase
    {
        public static ObservableCollection<CustomerViewModel> CustomerList { get; set; } 

        #region Constructor

        public MainWindowModel(List<Customer> books)
        {
            CustomerList = new ObservableCollection<CustomerViewModel>(books.Select(b => new CustomerViewModel(b)));
        }

        #endregion

        public static void Clear(int _id)
        {
            foreach (var item in CustomerList.ToList())
            {
                if(item.Id == _id)
                {
                    CustomerList.Remove(item);
                }
            }
        }

        public static void AddNew()
        {
            CustomerList.Add(new CustomerViewModel(new Customer()));
        }

        public static void Add(CustomerViewModel cus)
        {
            CustomerList.Add(cus);
        }
    }
}
