﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using KPZ_Lab6.Models;
using KPZ_Lab6.Commands;
using System.Windows.Input;
using System.Media;
using System.Windows;

namespace KPZ_Lab6.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {
        public Customer customer;

        static public List<Customer> list;

        public CustomerViewModel(Customer _customer)
        {
            this.customer = _customer;
        }

        public int Id
        {
            get { return customer.id; }
            set
            {
                customer.id = value;
                //OnPropertyChanged("Name");
            }
        }

        public string Name
        {
            get { return customer.name; }
            set
            {
                customer.name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return customer.surname; }
            set
            {
                customer.surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public int Age
        {
            get {  if (customer.age.HasValue) return customer.age.Value;
                else return -1; }
            set
            {
                if (value.ToString() != "")
                {
                    customer.age = value;
                    OnPropertyChanged("Age");
                }
            }
        }

        public int ExursionsOrdered
        {
            get
            {
                if (customer.exursions_ordered.HasValue) return customer.exursions_ordered.Value;
                else return -1;
            }
            set
            {
                if (value.ToString() != "")
                {
                    customer.exursions_ordered = value;
                    OnPropertyChanged("ExursionsOrdered");
                }
            }
        }

        #region Commands

        #region 
        private DelegateCommand clearCommand;

        public ICommand ClearCommand
        {
            get
            {
                if (clearCommand == null)
                {
                    clearCommand = new DelegateCommand(Clear, CanClear);
                }
                return clearCommand;
            }
        }

        private void Clear()
        {
            using (var context = new MyDBContext())
            {
                foreach (var item in context.Customers)
                {
                    if (item.id == Id)
                        context.Customers.Remove(item);
                }
                context.SaveChanges();

            }
            MainWindowModel.Clear(Id);
        }

        private bool CanClear()
        {
            return Age > -2 || ExursionsOrdered > -2 || Name != "" || Surname != "";
        }

        #endregion

        #region 
        private DelegateCommand PlayCommand;

        public ICommand playCommand
        {
            get
            {
                if (PlayCommand == null)
                {
                    PlayCommand = new DelegateCommand(Play);
                }
                return PlayCommand;
            }
        }

        private void Play()
        {
            using (var context = new MyDBContext())
            {
                Customer cus = new Customer();
                foreach (var item in context.Customers)
                {
                    if (item.id == Id)
                        cus = context.Customers.Find(Id);
                }
                cus.name = Name;
                cus.surname = Surname;
                if (Age > -1)cus.age = Age;
                if (ExursionsOrdered > -1) cus.exursions_ordered = ExursionsOrdered;
                context.SaveChanges();

            }

        }

        #endregion

        #region 
        private DelegateCommand AddCommand;

        public ICommand addCommand
        {
            get
            {
                if (AddCommand == null)
                {
                    AddCommand = new DelegateCommand(Add);
                }
                return AddCommand;
            }
        }

        private void Add()
        {
            using (var context = new MyDBContext())
            {
                Customer cus = new Customer();
                cus.name = Name;
                cus.surname = Surname;
                if (Age > -1) cus.age = Age;
                if (ExursionsOrdered > -1) cus.exursions_ordered = ExursionsOrdered;
                foreach (var item in MainWindowModel.CustomerList.ToList())
                {
                    if(cus.name == item.Name && cus.surname == item.Surname)
                    {
                        MessageBox.Show("This Customer already exist");
                        return;
                    }
                }
                context.Customers.Add(cus);
                context.SaveChanges();
               // MainWindowModel.Add(new CustomerViewModel(cus));

            }

        }

        private DelegateCommand AddNewCommand;

        public ICommand addNewCommand
        {
            get
            {
                if (AddNewCommand == null)
                {
                    AddNewCommand = new DelegateCommand(AddNew);
                }
                return AddNewCommand;
            }
        }

        private void AddNew()
        {
            MainWindowModel.AddNew();
        }

        #endregion
        #endregion*/
    }
}
