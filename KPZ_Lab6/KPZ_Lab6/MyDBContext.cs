﻿
using System.Data.Entity;


namespace KPZ_Lab6
{
    class MyDBContext : DbContext
    {
        public MyDBContext() : base("DbConnection")
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Exursion> Exursions { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}
