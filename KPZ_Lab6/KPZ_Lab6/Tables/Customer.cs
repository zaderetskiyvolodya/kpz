﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab6
{
    public class Customer
    {
        

        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int? age { get; set; }
        public int? exursions_ordered { get; set; }

        public Customer()
        {
            
            this.name = "";
            this.surname = "";
            this.age = -1;
            this.exursions_ordered = -1;
        }

        /*public Customer( string name, string surname, int age)
        {
           
            this.name = name;
            this.surname = surname;
            this.age = age;
        }

        public Customer(string name, string surname)
        {

            this.name = name;
            this.surname = surname;
        }*/

    }

}
