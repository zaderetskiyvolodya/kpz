﻿
using System.ComponentModel.DataAnnotations;


namespace KPZ_Lab6
{
    class Exursion
    {
        [Key]
        public string name { get; set; }

        public int location_id { get; set; }
        public float price { get; set; }
        public int duration { get; set; }
    }
}
