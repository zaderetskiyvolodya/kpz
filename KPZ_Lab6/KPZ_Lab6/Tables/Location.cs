﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab6
{
    class Location
    {
        public int id { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }
        public float? longtitude { get; set; }
        public float? latitude { get; set; }

    }
}
