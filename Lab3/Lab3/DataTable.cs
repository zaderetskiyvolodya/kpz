﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    class DataTable
    {
        public List<Car> Cars {
            get {
                return new List<Car>()
                 {
                    new Car(001, "Ford", "Raptor", 90000 ),
                    new Car(002, "Audi", "A6", 50000 ),
                    new Car(002, "Ford", "Mondeo",35000 ),
                    new Car(002, "BMW", "535", 50000 ),
                    new Car(002, "Audi", "A4", 30000 ),
                    new Car(003, "Audi", "Q7", 60000 ),
                    new Car(003, "Jeep", "Grand Cherokee", 40000 ),
                    new Car(003, "BMW", "X5", 62000 ),
                    new Car(004, "Doodge", "Challanger", 40000 ),
                    new Car(004, "Chevrolet", "Camarro", 40000 )
                 };
            } }

        public List<Category> Categories
        {
            get
            {
                return new List<Category>()
                {
                    new Category(001,"Pickup"),
                    new Category(002,"Sedan"),
                    new Category(003,"Crossover"),
                    new Category(004,"Muscle car"),
                };
            }
        }


        public string PrintCars(List<Car> cars)
        {
            StringBuilder str = new StringBuilder();
            foreach (var car in cars)
            {
                str.Append(String.Format("Category Id: {0}  Car: {1}  {2}  Price: {3} \n ", car.CategoryId, car.Brand, car.Model, car.Price));
            }
            return str.ToString();
        }

        public string PrintCategories(List<Category> categories)
        {
            StringBuilder str = new StringBuilder();
            foreach (var category in categories)
            {
                str.Append(String.Format("Category Id: {0}  Name: {1}  \n ", category.Id, category.Name));
            }
            return str.ToString();
        }

        public Dictionary<Category, List<Car>> CreateDictionary()
        {
            Dictionary<Category, List<Car>> dictionary = new Dictionary<Category, List<Car>>();
            foreach (var category in Categories)
            {
                List<Car> cars = new List<Car>();
                foreach (var car in Cars)
                {
                    if (category.Id == car.CategoryId)
                    {
                        cars.Add(car);
                    }
                }
                dictionary.Add(category, cars);
            }
            return dictionary;
        }

        public Dictionary<Category, List<Car>> SortListInDictionary(Dictionary<Category, List<Car>> dictionary)
        {
            foreach (var dic in dictionary)
            {
                dic.Value.Sort(delegate (Car x, Car y)
                {
                    if (x.Brand == null && y.Brand == null) return 0;
                    else if (x.Brand == null) return -1;
                    else if (y.Brand == null) return 1;
                    else return x.Brand.CompareTo(y.Brand);
                });
            }
            return dictionary;
        }

        public string PrintDictionary(Dictionary<Category, List<Car>> dic)
        {
            StringBuilder str = new StringBuilder();
            foreach (var item in dic)
            {
                str.Append(String.Format("\nCategory: {0} \n", item.Key.Name));
                foreach (var car in item.Value)
                {
                    str.Append(String.Format("          {0} {1} \n", car.Brand, car.Model));
                }
            }
            return str.ToString();
        }

        public List<Car> SortCarsByBrand()
        {
            List<Car> cars = Cars;
            cars.Sort(delegate (Car x, Car y)
            {
                if (x.Brand == null && y.Brand == null) return 0;
                else if (x.Brand == null) return -1;
                else if (y.Brand == null) return 1;
                else return x.Brand.CompareTo(y.Brand);
            });
            return cars;
        }


        public List<Category> SortCategoryByCarAmount()
        {
            List<Category> categories = Categories;
            categories.Sort(delegate (Category x, Category y)
            {
                int xCount = 0, yCount = 0;
                foreach (var car in Cars)
                {
                    if (car.CategoryId == x.Id) xCount++;
                    if (car.CategoryId == y.Id) yCount++;
                }
                return xCount.CompareTo(yCount);
            });
            return categories;
        }

        public Type[] ConvertToArray(List<Type> list)
        {
            return list.ToArray();
        }

        public string SelectExpensiveCars()
        {
            StringBuilder str = new StringBuilder();
            var selectedCars = from car in Cars
                               where car.Price > 50000
                               select car;
            foreach (var item in selectedCars)
            {
                str.Append(String.Format("Car: {0}  {1}  Price: {2} \n ", item.Brand, item.Model, item.Price));
            }
            return str.ToString();
        }

        public string GroupCarsByBrand()
        {
            StringBuilder str = new StringBuilder();
            var carGroups = from car in Cars
                              group car by car.Brand;

            foreach (IGrouping<string, Car> g in carGroups)
            {
                str.Append("\n\n"+g.Key);
                foreach (var t in g)
                    str.Append("\n"+t.Model);
            }
            return str.ToString();
        }


    }
}
