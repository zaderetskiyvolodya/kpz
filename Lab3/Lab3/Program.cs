﻿using System;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dt = new DataTable();
            Console.WriteLine(dt.PrintDictionary(dt.CreateDictionary()));
            Console.WriteLine();
            Console.WriteLine(dt.PrintDictionary(dt.SortListInDictionary(dt.CreateDictionary())));
            Console.WriteLine();
            Console.WriteLine(dt.PrintCars(dt.SortCarsByBrand()));
            Console.WriteLine();
            Console.WriteLine(dt.PrintCategories(dt.SortCategoryByCarAmount()));
            Console.WriteLine();
            Console.WriteLine(dt.GroupCarsByBrand());
            Console.WriteLine();
            Console.WriteLine(dt.SelectExpensiveCars());
        }
    }
}
