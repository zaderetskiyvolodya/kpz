﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab3
{
    class Car
    {
        public int CategoryId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Price { get; set; }

        public Car(int _categoryId, string _brand, string _model, int _price)
        {
            CategoryId = _categoryId;
            Brand = _brand;
            Model = _model;
            Price = _price;
        }
    }
}
