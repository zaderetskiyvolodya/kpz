﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab3
{
    class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Category(int _id, string _name)
        {
            Id = _id;
            Name = _name;
        }
    }
}
