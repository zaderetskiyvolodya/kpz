﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;
using ZaderetskiyVolodymyr.RobotChallange;

namespace ZaderetskyiVolodymyr.RobotChallange.Test
{
    [TestClass]
    class AlgorithmTest
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(4, 5) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCreateRobotCommand()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(4, 5) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }


        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestIsCellFreeCommand()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };
            Position pos = new Position(3,3);

            Assert.IsTrue(algorithm.IsCellFree(pos,robot,robots));
        }


        [TestMethod]
        public void TestIsStationFreeCommand()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };
            Position pos = new Position(3, 3);

            Assert.IsTrue(algorithm.IsStationFree(map.Stations[0], robot, robots));
        }

        [TestMethod]
        public void TestFindNearestStation()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) };


            Assert.AreEqual(map.Stations[0], algorithm.FindNearestFreeStation( robot, map, robots));
        }

        [TestMethod]
        public void TestFindEnemyStation()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2) , OwnerName = "my"};


            Assert.AreEqual(robots[0], algorithm.FindNearestEnemyRobotToAtack(robot,robots));
        }

        [TestMethod]
        public void TestGetRobotsOnDistance()
        {
            var algorithm = new ZaderetskiyVolodymyrAlgorithm();
            var map = new Map();
            var stationPosition = new Position(3, 1);
            map.Stations.Add(new EnergyStation() { Position = stationPosition, Energy = 200, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 700, Position = new Position(1, 1) } };
            var robot = new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 2), OwnerName = "my" };


            Assert.AreEqual(robots, algorithm.GetRobotsAtDistance(5,robot,robots));
        }


    }
}
