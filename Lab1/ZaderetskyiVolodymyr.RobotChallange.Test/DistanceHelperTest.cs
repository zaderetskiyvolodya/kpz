using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using ZaderetskiyVolodymyr.RobotChallange;

namespace ZaderetskyiVolodymyr.RobotChallange.Test
{
    [TestClass]
    public class DistanceHelperTest
    {
        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(4, 5);
            Assert.AreEqual(25, DistanceHelper.FindDistance(p1, p2));
        }

        [TestMethod]
        public void TestCloserCellToRobot()
        {
            var robot = new Position(1, 1);
            var station = new Position(4, 2);
            var pos = new Position(2, 1);

            Assert.AreEqual(pos, DistanceHelper.FindCloserCellToRobot(robot,station));
        }
    }
}
