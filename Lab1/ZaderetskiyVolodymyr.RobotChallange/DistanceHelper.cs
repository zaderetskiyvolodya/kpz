﻿using System;
using System.Collections.Generic;
using System.Text;
using Robot.Common;

namespace ZaderetskiyVolodymyr.RobotChallange
{
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public static Position FindCloserCellToRobot(Position robot, Position station)
        {
            Position cellCloserToRobot = new Position();
            if (Math.Abs(robot.X - station.X) < 2)
            {
                cellCloserToRobot.X = robot.X;
            }
            else
            {
            cellCloserToRobot.X = station.X + 2 * ((robot.X - station.X) / Math.Abs(robot.X - station.X));
            }
            if (Math.Abs(robot.Y - station.Y) < 2)
            {
                cellCloserToRobot.Y = robot.Y;
            }
            else
            {
                cellCloserToRobot.Y = station.Y + 2 * ((robot.Y - station.Y) / Math.Abs(robot.Y - station.Y));
            }
            return cellCloserToRobot;
        }
    }
}
