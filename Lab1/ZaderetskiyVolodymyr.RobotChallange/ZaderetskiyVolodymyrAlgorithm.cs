﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZaderetskiyVolodymyr.RobotChallange
{
    public class ZaderetskiyVolodymyrAlgorithm : IRobotAlgorithm
    {
        public string Author => "Zaderetskiy Volodymyr";
        private int RoundCount { get; set; }
        private int myRobotsCount { get; set; }
        public ZaderetskiyVolodymyrAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
            myRobotsCount = 10;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            bool attack = false;
            if ((movingRobot.Energy > 400) && (myRobotsCount < 100) && (RoundCount<40))
            {
                myRobotsCount++;
                return new CreateNewRobotCommand();
            }

            EnergyStation station = FindNearestStation(robots[robotToMoveIndex], map, robots);

            Robot.Common.Robot robotToAttack = FindNearestEnemyRobotToAtack(movingRobot, robots);
            if(station!= null && robotToAttack != null)
            {

            attack = station.Energy < (int)(robotToAttack.Energy * 0.1f) - (DistanceHelper.FindDistance(movingRobot.Position, robotToAttack.Position));
            }

            if ((station != null) && (Math.Abs(station.Position.X - movingRobot.Position.X)<3) && (Math.Abs(station.Position.Y - movingRobot.Position.Y) < 3) && (station.Energy>30))
            {
                if(robotToAttack != null)
                {
                    attack = station.Energy < (int)(robotToAttack.Energy * 0.1f) - (DistanceHelper.FindDistance(movingRobot.Position, robotToAttack.Position)+30);
                }
                if (!attack)
                {
                return new CollectEnergyCommand();
                }
            }else if(robotToAttack!=null && attack && (movingRobot.Energy > (30 + DistanceHelper.FindDistance(movingRobot.Position,robotToAttack.Position)))){

                return new MoveCommand() { NewPosition = robotToAttack.Position };
            }
            else
            {
                Position cellCloserToRobot = DistanceHelper.FindCloserCellToRobot(movingRobot.Position,station.Position);
                if (station.Energy>(DistanceHelper.FindDistance(movingRobot.Position, cellCloserToRobot) * 2) && movingRobot.Energy> DistanceHelper.FindDistance(movingRobot.Position, cellCloserToRobot))
                {
                return new MoveCommand() { NewPosition = cellCloserToRobot };
                }
                else
                {
                    Position newPosition = new Position();
                    if (movingRobot.Position.X == station.Position.X)
                    {
                        newPosition.X = movingRobot.Position.X;
                    }
                    else
                    {
                    newPosition.X += (movingRobot.Position.X - station.Position.X) / Math.Abs(movingRobot.Position.X - station.Position.X);
                    }
                    if (movingRobot.Position.Y == station.Position.Y)
                    {
                        newPosition.Y = movingRobot.Position.Y;
                    }
                    else
                    {
                        newPosition.Y += (movingRobot.Position.Y - station.Position.Y) / Math.Abs(movingRobot.Position.Y - station.Position.Y);
                    }
                    return new MoveCommand() { NewPosition = newPosition };
                }
            }
            Position pos = new Position(movingRobot.Position.X+1,movingRobot.Position.Y+1);
            return new MoveCommand() { NewPosition = pos };
        }

        public EnergyStation FindNearestStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            IList<EnergyStation> stations = map.Stations;
            EnergyStation temp = new EnergyStation() { Energy = 100};
            for (int i = 1; i < stations.Count; i++)
            {
                if (stations.Count > 0)
                {
                    foreach (var station in stations)
                    {
                            int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                            if (d < minDistance)
                            {
                                minDistance = d;
                                nearest = station;
                            }
                    }
                    if (nearest.Energy > 200 || nearest.Energy>temp.Energy )
                    {
                        return nearest == null ? null : nearest;
                    }
                    temp = nearest;
                    stations.Remove(nearest);
                }
            }
            return temp;
        }

        public Robot.Common.Robot FindNearestEnemyRobotToAtack(Robot.Common.Robot movingRobot,  IList<Robot.Common.Robot> robots)
        {
            Robot.Common.Robot nearest = null;
            int minDistance = int.MaxValue;
            List<Robot.Common.Robot> robotsAtDistance =  GetRobotsAtDistance(10,movingRobot,robots);
            for (int i = 0; i < robotsAtDistance.Count; i++)
            {
                if (robotsAtDistance!=null)
                {
                    foreach (var robot in robotsAtDistance)
                    {
                        if (robot.OwnerName != movingRobot.OwnerName)
                        {
                            int d = DistanceHelper.FindDistance(robot.Position, movingRobot.Position);
                            if (d < minDistance)
                            {
                                minDistance = d;
                                nearest = robot;
                            }
                        }

                    }

                    if ((int)(nearest.Energy * 0.1f) > (DistanceHelper.FindDistance(movingRobot.Position, nearest.Position)+50))
                    {
                        return nearest;
                    }
                    robotsAtDistance.Remove(nearest);
                }
            }
            return null;

        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public List<Robot.Common.Robot> GetRobotsAtDistance(int distance, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            List<Robot.Common.Robot> robotsAtDistance = new List<Robot.Common.Robot>();
            int tempDistance = int.MinValue;
            foreach(var robot in robots)
            {
                if (robot.Position != movingRobot.Position)
                {
                    tempDistance = DistanceHelper.FindDistance(movingRobot.Position, robot.Position);
                    if ((int)Math.Sqrt(tempDistance) < tempDistance)
                    {
                        robotsAtDistance.Add(robot);
                    }
                }
            }
            return robotsAtDistance;
        }
    }
}
