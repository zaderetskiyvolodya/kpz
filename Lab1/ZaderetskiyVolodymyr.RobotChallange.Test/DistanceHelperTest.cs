using Microsoft.VisualStudio.TestTools.UnitTesting;
//using NUnit.Framework;
using Robot.Common;

namespace ZaderetskiyVolodymyr.RobotChallange.Test
{
    [TestClass]
    public class DistanceHelperTest
    {

        [TestMethod]
        public void FindDistanceTest()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(4, 5);
            Assert.AreEqual(25, DistanceHelper.FindDistance(p1,p2));
        }
    }
}