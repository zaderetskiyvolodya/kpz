﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPZ_Lab2
{
    class Storage
    {
        public delegate void StorageHandler(string message);
        public delegate void StorageHandlerArgs(object sender, StorageEventArgs e);
        public event StorageHandler Notify;
        public event StorageHandlerArgs NotifyArgs;

        private int amount;
        public int Amount { get { return amount; } }

        public Storage(int _amount)
        {
            amount = _amount;
            Console.WriteLine($"Отримали в спадок склад iз {_amount} мiшками цибулi\n");
        }

        public void Put(int _amount)
        {
            amount += _amount;
            if (Notify != null) Notify($"Привезли {_amount} мiшкiв цибулi \nВсього на складi {amount} мiшкiв цибулi\n");
        }

        public void Take(int _amount)
        {
            if (amount >= _amount)
            {
                amount -= _amount;
                if (Notify != null) Notify($"Забрали {_amount} мiшкiв цибулi \nВсього на складi {amount} мiшкiв цибулi\n");
            }
            else
            {
                if (NotifyArgs != null) NotifyArgs(this, new StorageEventArgs($"Неможливо забрати {_amount} мiшкiв цибулi \nВсього на складi {amount} мiшкiв цибулi\n"));
            }
        }
    }
}
