﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPZ_Lab2
{
    class StorageEventArgs : EventArgs
    {
        public string message { get; }

        public StorageEventArgs(string mes)
        {
            message = mes;
        }
    }
}
