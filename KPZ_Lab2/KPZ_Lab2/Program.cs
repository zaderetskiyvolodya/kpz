﻿using System;

namespace KPZ_Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Storage storage = new Storage(100);
            storage.Notify += DisplayMessage;
            storage.NotifyArgs += DisplayMessageArgs;
            storage.Put(50);
            storage.Notify -= DisplayMessage;
            storage.Notify += mes => Console.WriteLine(mes);
            storage.Take(80);
            storage.Take(90); 
        }

        public static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
        public static void DisplayMessageArgs(object sender, StorageEventArgs e)
        {
            Console.WriteLine(e.message);
        }
    }
}
