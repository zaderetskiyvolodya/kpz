﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    // поля та класи без модифікаторів доступу
    public interface IEmployee
    {
        string name { get; set; }
        int salary { get; set; }
        string position { get; set; }        

    }
}
