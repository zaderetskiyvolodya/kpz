﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    //ініціалізації полів як статичних так і динамічних
    static class ExursionAgency
    {
        //внутрішній клас з доступом меншим за public
        static private int numOfExursion;
        static private int revenue;
        public class CashRegisters {
            public List<Cashier> cashiers;
            private int numOfRegister;
        }
        //перевантаження функції
        static public int CountWorkers(CashRegisters CR) 
        {
            return 0;
        }
        static public int CountWorkers(List<CashRegisters> LRC)
        { 
            return 0;
        }

       static public void Show()
        {
            Agency.Make();
        }

        //Тільки в середині цього класу
        private class Agency
        {
            public List<Cashier> cashiers;
            private int numOfRegister;

            static public void Make() { }
        }
    }
}
