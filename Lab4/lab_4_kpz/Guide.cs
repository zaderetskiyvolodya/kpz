﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace lab_4_kpz
{
    class Guide : Employee
    {
        private int exursionsProvided { get; set; }

        public int firstField { get; set; } = 1;

        protected int secondField { get; set; } = 2;

        internal int thirdField { get; set; } = 3;

        public Guide(string _name, int _salary, string _position, int _exursionsProvided) :
            base(_name, _salary, _position)
        {
            exursionsProvided = _exursionsProvided;
        }
        //base
        public void DoSmthg() {
            base.DoSmthg();
        }

        public override void DoSmthg1()
        {
            return;
        }
    }
}
