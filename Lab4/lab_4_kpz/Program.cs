﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace lab_4_kpz
{
    class Program
    {
        static void Main(string[] args)
        {
            // ланцюжок наслідування у якому б був звичайний клас, абстрактний клас та інтерфейс
            Guide guide = new Guide("Petro Schur", 1200, "high", 6);  
            Console.WriteLine(guide.name);

            //булівські операції на перелічуваних типах(^,||, &&. &, |,…) // pobitovi
            DayOfWeek workDay = DayOfWeek.Tuesday | DayOfWeek.Wednesday | DayOfWeek.Thursday; // 1110
            DayOfWeek workDay1 = DayOfWeek.Friday & DayOfWeek.Saturday | DayOfWeek.Monday; // 1001
            bool workOnTuesday = (workDay | DayOfWeek.Monday) == DayOfWeek.Sunday;
            bool workOnTuesday2 = ((workDay1 | DayOfWeek.Wednesday) & DayOfWeek.Saturday) == DayOfWeek.Saturday;
            Console.WriteLine("I guessed? {0}, {1}", workOnTuesday, workOnTuesday2);
            Console.WriteLine();

            //перевантаження
            ExursionAgency.CashRegisters cr = new ExursionAgency.CashRegisters();
            ExursionAgency.CountWorkers(cr);
            List<ExursionAgency.CashRegisters> crl = new List<ExursionAgency.CashRegisters>();
            ExursionAgency.CountWorkers(crl);

            // method and static method
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for(int i = 0; i < 100000000; i++)
            {
                VIPClient.MakeOrder();
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for static method " + elapsedTime);

            Client client = new Client();
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                client.MakeOrder();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for ordinary method " + elapsedTime);

            //Dostup do metodiv
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                client.OrderSmth();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Access RunTime for ordinary method " + elapsedTime);

            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000000; i++)
            {
                Client.OrderSmth1();
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Access RunTime for static method " + elapsedTime);

            //static and ord vars
            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000; i++)
            {
                Client.nameNew += "+";
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for static var " + elapsedTime);

            stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100000; i++)
            {
                client.name += "+";
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}.{1:00}",
            ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("RunTime for ordinary var " + elapsedTime);

            ////////// boxing unboxing
            List<object> mixedList = new List<object>();

            mixedList.Add("Work days: ");
            mixedList.Add(DayOfWeek.Monday);
            mixedList.Add(DayOfWeek.Friday);

            mixedList.Add("Weekends: ");
            mixedList.Add(DayOfWeek.Sunday);
            for (int j = 5; j < 10; j++)
            {
                mixedList.Add(j);
            }
            foreach (var item in mixedList)
            {
                Console.WriteLine(item);
            }
            var testSum = 0;
            for (var j = 5; j < 10; j++)
            {
                testSum += (int)mixedList[j] * (int)mixedList[j];
            }
            Console.WriteLine("Sum: " + testSum);

            Console.ReadLine();
        }
    }
}
