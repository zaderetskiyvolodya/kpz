﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    class UniversalWorker : IEmployee, IManager
    {
        public string name { get; set; }
        public int salary { get; set; }
        public string position { get; set; }

        public int yearsOfExpierence { get; set; }
    }
}
