﻿

namespace lab_4_kpz
{
    //перелічуваний тип
    public enum DayOfWeek
    {
        Monday = 1,     //0001
        Tuesday = 2,    //0010
        Wednesday = 4,  //0100
        Thursday = 8,   //1000
        Friday = 9,     //1001
        Saturday = 12,  //1100
        Sunday = 15     //1111
    }
}
