﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_4_kpz
{
    /*Статичні класи.Порівняти час створення і потреби стеку для викликів методів статичного та 
            звичайного класів. 
            Порівняти час виконання для звичайних та  статичних методів.  
            Порівняти час доступу та операцій над звичайними та статичними змінними.*/
    static class VIPClient
    {
        public static string name = "Misko";
        public static int clientVipId = 1004;
        public static int discountCardId = 2008;
        public static float MakeOrder() 
        {
            float koef = 0.4f;
            float price = koef * (discountCardId / 4);
            return price;
        }
    }

    class Client 
    {
        public string name = "Petro";
        public int clientId = 3012;
        public int discountCardId = 2012;
        public static string nameNew = "Petro";
        public float MakeOrder()
        {
            float koef = 0.4f;
            float price = koef * (discountCardId / 4);
            return price;
        }

        public void OrderSmth() { }
        public static void OrderSmth1() { }
    } 
}
